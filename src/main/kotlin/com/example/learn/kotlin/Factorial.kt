package com.example.learn.kotlin

object Factorial {
    private lateinit var fact: (Int) -> Int
    init {
        fact = {n -> if (n <= 1) n else n * factorial(n - 1)}
    }
    val factorial = fact
}

object Factorial2 {
    val factorial: (Int) -> Int by lazy { {n: Int -> if (n <= 1) n else n * factorial(n - 1)} }
}
