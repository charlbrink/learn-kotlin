package com.example.learn.kotlin

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class FactorialTest {
    private val factorial = Factorial.factorial
    private val factorial2 = Factorial2.factorial

    @Test
    fun `given 1 when calling Factorial then expect 1`() {
        assertThat(factorial(1)).isEqualTo(1)
    }

    @Test
    fun `given 4 when calling Factorial then expect 24`() {
        assertThat(factorial(4)).isEqualTo(24)
    }

    @Test
    fun `given 1 when calling Factorial2 then expect 1`() {
        assertThat(factorial2(1)).isEqualTo(1)
    }

    @Test
    fun `given 4 when calling Factorial2 then expect 24`() {
        assertThat(factorial2(4)).isEqualTo(24)
    }

}